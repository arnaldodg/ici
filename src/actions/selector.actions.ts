import { Action } from '@ngrx/store';
import { Selector } from '../models/selector.model';

export const BUILD = '[Selector] BUILD';
export const CLICK = '[Selector] CLICK';
export const RESET = '[Selector] RESET';


export class Build implements Action {
  readonly type = BUILD;
  
  constructor(public payload: string){
  
  }
}

export class Click implements Action {
  readonly type = CLICK;
  
  constructor(public payload: string){
  
  }
}

export class Reset implements Action {
  readonly type = RESET;

  constructor(public payload: string){
  
  }
}


export type All
 = Build
 | Click
 | Reset
 