import { Action } from '@ngrx/store';
import { Proposal } from '../models/proposal.model';
import { Client } from '../models/client.model';
import { Estate } from '../models/estate.model';

import { CloneVisitor } from '@angular/compiler/src/i18n/i18n_ast';


//
// USAR PARA ACTIONS TRANSVERSAIS
//

// ACTIONs keys
export const PROPOSAL_YIELD = '[Client] YIELD';

// Actions Classes
export class ProposalYield implements Action {
  readonly type = PROPOSAL_YIELD;

  constructor(public payload: Proposal){

  }
}

export type All
 = ProposalYield       //