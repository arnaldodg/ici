import { Action } from '@ngrx/store';
import { OpParams } from '../models/op-params.model';

export const LOAD = '[OpFormParams] LOAD';
export const LOAD_SUCCESS = '[OpFormParams] LOAD_SUCCESS';
export const LOAD_FAIL = '[OpFormParams] LOAD_FAIL';

export class Load implements Action {
  readonly type = LOAD;  
  
}

export class LoadSuccess implements Action {
  readonly type = LOAD_SUCCESS;
  
  constructor(public payload: any){
  
  }
}

export class LoadFail implements Action {
  readonly type = LOAD_FAIL;

  constructor(public payload: any){
  
  }
}

export type All
 = Load
 | LoadSuccess
 | LoadFail
