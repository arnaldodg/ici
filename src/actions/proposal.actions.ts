import { Action } from '@ngrx/store';
import { Proposal } from '../models/proposal.model';
import { CloneVisitor } from '@angular/compiler/src/i18n/i18n_ast';


// ACTIONs keys
export const LOAD = '[Proposal] LOAD';
export const LOAD_SUCCESS = '[Proposal] LOAD_SUCCESS';
export const LOAD_FAIL = '[Proposal] LOAD_FAIL';

export const SELECT = '[Proposal] SELECT'; // OPEN?!

export const NEW = '[Proposal] NEW';
export const CLONE = '[Proposal] CLONE';

export const SAVE = '[Proposal] SAVE';
export const SAVE_SUCCESS = '[Proposal] SAVE_SUCCESS';
export const SAVE_FAIL = '[Proposal] SAVE_FAIL';

export const SEND = '[Proposal] SEND';
export const SHARE = '[Proposal] SEND';


// Actions Classes
export class Load implements Action {
  readonly type = LOAD;  
  
  constructor(public payload: any){
  
  } 
}

export class LoadSuccess implements Action {
  readonly type = LOAD_SUCCESS;
  
  constructor(public payload: any){
  
  }
}

export class LoadFail implements Action {
  readonly type = LOAD_FAIL;

  constructor(public payload: any){
  
  }
}

export class Select implements Action {
  readonly type = SELECT;

  constructor(public payload: any){

  }
}

export class New implements Action {
  readonly type = NEW;

  constructor(public payload: Proposal){

  }
}

export class Clone implements Action {
  readonly type = CLONE;

  constructor(public payload: any){

  }
}

export class Save implements Action {
  readonly type = SAVE;

  constructor(public payload: any){

  }
}

export class SaveSuccess implements Action {
  readonly type = SAVE_SUCCESS;

  constructor(public payload: any){

  }
}

export class SaveFail implements Action {
  readonly type = SAVE_FAIL;

  constructor(public payload: any){

  }
}

export type All
 = Load           // Load All Proposals from storage
 | LoadSuccess    // 
 | LoadFail       //
 | Select         // Select a Proposal to work on
 | New            // Create New Empty Proposal
 | Clone          // Create New Proposal using another Proposal info
 | Save           // Save/Update a single Proposal (selected Proposal)
 | SaveSuccess    //
 | SaveFail       //