import { Action } from '@ngrx/store';
import { Client } from '../models/client.model';
import { CloneVisitor } from '@angular/compiler/src/i18n/i18n_ast';


// ACTIONs keys
export const LOAD = '[Client] LOAD';
export const LOAD_SUCCESS = '[Client] LOAD_SUCCESS';
export const LOAD_FAIL = '[Client] LOAD_FAIL';

export const SELECT = '[Client] SELECT'; // OPEN?!

export const NEW = '[Client] NEW';
export const CLONE = '[Client] CLONE';

export const SAVE = '[Client] SAVE';
export const SAVE_SUCCESS = '[Client] SAVE_SUCCESS';
export const SAVE_FAIL = '[Client] SAVE_FAIL';

// Actions Classes
export class Load implements Action {
  readonly type = LOAD;  
  
  constructor(public payload: any){
  
  } 
}

export class LoadSuccess implements Action {
  readonly type = LOAD_SUCCESS;
  
  constructor(public payload: any){
  
  }
}

export class LoadFail implements Action {
  readonly type = LOAD_FAIL;

  constructor(public payload: any){
  
  }
}

export class Select implements Action {
  readonly type = SELECT;

  constructor(public payload: any){

  }
}

export class New implements Action {
  readonly type = NEW;

  constructor(public payload: any){

  }
}

export class Clone implements Action {
  readonly type = CLONE;

  constructor(public payload: any){

  }
}

export class Save implements Action {
  readonly type = SAVE;

  constructor(public payload: any){

  }
}

export class SaveSuccess implements Action {
  readonly type = SAVE_SUCCESS;

  constructor(public payload: any){

  }
}

export class SaveFail implements Action {
  readonly type = SAVE_FAIL;

  constructor(public payload: any){

  }
}

export type All
 = Load           // Load All Clients from storage
 | LoadSuccess    // 
 | LoadFail       //
 | Select         // Select a Client to work on
 | New            // Create New Empty Client
 | Clone          // Create New Client using another Client info
 | Save           // Save/Update a single Client (selected Client)
 | SaveSuccess    //
 | SaveFail       //