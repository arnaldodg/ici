import { Action } from '@ngrx/store';
import { OpParams } from '../models/op-params.model';

export const LOAD = '[OpFormParams] LOAD';
export const LOAD_SUCCESS = '[OpFormParams] LOAD_SUCCESS';
export const LOAD_FAIL = '[OpFormParams] LOAD_FAIL';

export const SET_SELECTOR_OPTION = '[OpFormParams] SET_SELECTOR_OPTION';

export const SELECT_OP = '[OpFormParams] SELECT_OP';

export class Load implements Action {
  readonly type = LOAD;  
  
}

export class LoadSuccess implements Action {
  readonly type = LOAD_SUCCESS;
  
  constructor(public payload: any){
  
  }
}

export class LoadFail implements Action {
  readonly type = LOAD_FAIL;

  constructor(public payload: any){
  
  }
}

export class SetSelectorOption implements Action {
  readonly type = SET_SELECTOR_OPTION;

  constructor(public payload: {group: string; option: string}){
  
  }
}

export class SelectOp implements Action {
  readonly type = SELECT_OP;

  constructor(public payload: any){
  
  }
}

export type All
 = Load
 | LoadSuccess
 | LoadFail
 | SetSelectorOption
 | SelectOp
