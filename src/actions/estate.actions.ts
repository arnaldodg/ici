import { Action } from '@ngrx/store';
import { Estate } from '../models/estate.model';
import { CloneVisitor } from '@angular/compiler/src/i18n/i18n_ast';


// ACTIONs keys
export const LOAD = '[Estate] LOAD';
export const LOAD_SUCCESS = '[Estate] LOAD_SUCCESS';
export const LOAD_FAIL = '[Estate] LOAD_FAIL';

export const SELECT = '[Estate] SELECT'; // OPEN?!

export const NEW = '[Estate] NEW';
export const CLONE = '[Estate] CLONE';

export const SAVE = '[Estate] SAVE';
export const SAVE_SUCCESS = '[Estate] SAVE_SUCCESS';
export const SAVE_FAIL = '[Estate] SAVE_FAIL';

// Actions Classes
export class Load implements Action {
  readonly type = LOAD;  
  
  constructor(public payload: any){
  
  } 
}

export class LoadSuccess implements Action {
  readonly type = LOAD_SUCCESS;
  
  constructor(public payload: any){
  
  }
}

export class LoadFail implements Action {
  readonly type = LOAD_FAIL;

  constructor(public payload: any){
  
  }
}

export class Select implements Action {
  readonly type = SELECT;

  constructor(public payload: any){

  }
}

export class New implements Action {
  readonly type = NEW;

  constructor(public payload: any){

  }
}

export class Clone implements Action {
  readonly type = CLONE;

  constructor(public payload: any){

  }
}

export class Save implements Action {
  readonly type = SAVE;

  constructor(public payload: any){

  }
}

export class SaveSuccess implements Action {
  readonly type = SAVE_SUCCESS;

  constructor(public payload: any){

  }
}

export class SaveFail implements Action {
  readonly type = SAVE_FAIL;

  constructor(public payload: any){

  }
}

export type All
 = Load           // Load All Estates from storage
 | LoadSuccess    // 
 | LoadFail       //
 | Select         // Select a Estate to work on
 | New            // Create New Empty Estate
 | Clone          // Create New Estate using another Estate info
 | Save           // Save/Update a single Estate (selected Estate)
 | SaveSuccess    //
 | SaveFail       //