import { Action } from '@ngrx/store';
import { Registration } from '../models/registration.model';

export const REQUEST = '[Registration] REQUEST';
export const REQUEST_SUCCESS = '[Registration] REQUEST_SUCCESS';
export const REQUEST_FAIL = '[Registration] REQUEST_FAIL';
export const REQUEST_CANCEL = '[Registration] REQUEST_CANCEL';

export const CANCEL = '[Registration] CANCEL';

export const ACTIVATE = '[Registration] ACTIVATE';

export class Request implements Action {
  readonly type = REQUEST;  
  
}

export class RequestSuccess implements Action {
  readonly type = REQUEST_SUCCESS;
  
  constructor(public payload: any){
  
  }
}

export class RequestFail implements Action {
  readonly type = REQUEST_FAIL;

  constructor(public payload: any){
  
  }
}

export class RequestCancel implements Action {
  readonly type = REQUEST_CANCEL;

  constructor(public payload: any){
  
  }
}

export class Cancel implements Action {
  readonly type = CANCEL;

  constructor(public payload: any){
  
  }
}

export class Activate implements Action {
  readonly type = ACTIVATE;

  constructor(public payload: any){
  
  }
}

export type All
 = Request
 | RequestSuccess
 | RequestFail
 | RequestCancel
 | Cancel
 | Activate