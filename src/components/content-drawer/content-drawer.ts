import { 
  Component, 
  Input,
  ElementRef,
  Renderer
} from '@angular/core';

import { Platform, DomController } from 'ionic-angular';

@Component({
  selector: 'content-drawer',
  templateUrl: 'content-drawer.html'
})
export class ContentDrawer {

  @Input('options') options: any;
 
  handleHeight: number = 32;
  bounceBack: boolean = false;
  thresholdTop: number = 50;
  thresholdBottom: number = 50;
  hudHeight: number = 100;
 
  constructor(
    public element: ElementRef, 
    public renderer: Renderer, 
    public domCtrl: DomController, 
    public platform: Platform) {
 
  }
 
  ngAfterViewInit() {
 
    // init params
    if(this.options.handleHeight){
      this.handleHeight = this.options.handleHeight;
    }
 
    if(this.options.bounceBack){
      this.bounceBack = this.options.bounceBack;
    }
 
    if(this.options.thresholdFromBottom){
      this.thresholdBottom = this.options.thresholdFromBottom;
    }
 
    if(this.options.thresholdFromTop){
      this.thresholdTop = this.options.thresholdFromTop;
    }

    if(this.options.hudHeight){
      this.hudHeight = this.options.hudHeight;
    }
 
    // init geometry!
    this.renderer.setElementStyle(
      this.element.nativeElement, 
      'top', 
      this.platform.height() - (this.hudHeight) + 'px'
    );
    
    /*this.renderer.setElementStyle(
      this.element.nativeElement,
      'padding-top',
      this.handleHeight + 'px'
    );*/
    
    // Assign touch events (w Hammer)
    let hammer = new window['Hammer'](this.element.nativeElement);
    hammer.get('pan').set({ direction: window['Hammer'].DIRECTION_VERTICAL });
 
    hammer.on('pan', (ev) => {
      this.handlePan(ev);
    });
 
  }
 
  handlePan(ev){

    let newTop = ev.center.y;
    //console.log('newTop', newTop);
 
    let bounceToBottom = false;
    let bounceToTop = false;
 
    // should bounce top or bottom?!
    if(this.bounceBack && ev.isFinal){
 
      let topDiff = newTop - (this.thresholdTop + this.hudHeight);
      let bottomDiff = (this.platform.height() - (this.thresholdBottom + this.hudHeight)) - newTop;     
 
      topDiff >= bottomDiff ? bounceToBottom = true : bounceToTop = true;
    }
 
    if((newTop < this.thresholdTop && ev.additionalEvent === "panup") || bounceToTop){
 
      this.domCtrl.write(() => {
        this.renderer.setElementStyle(this.element.nativeElement, 'transition', 'top 0.5s');
        this.renderer.setElementStyle(this.element.nativeElement, 'top', '36px');
      });
 
    } else 
      if( ( (this.platform.height() - (newTop + this.hudHeight ) ) < this.thresholdBottom && ev.additionalEvent === "pandown") || bounceToBottom){
 
      this.domCtrl.write(() => {
        this.renderer.setElementStyle(this.element.nativeElement, 'transition', 'top 0.5s');
        this.renderer.setElementStyle(
          this.element.nativeElement,
          'top',
          this.platform.height() - (this.handleHeight + this.hudHeight - 48) + 'px');
      });
 
    } else { // dragging...!
 
      this.renderer.setElementStyle(this.element.nativeElement, 'transition', 'none');
 
      if(newTop > 0 && newTop < (this.platform.height()) ) {
 
        if(ev.additionalEvent === "panup" || ev.additionalEvent === "pandown"){
 
          this.domCtrl.write(() => {
            this.renderer.setElementStyle(this.element.nativeElement, 'top', (newTop) + 'px');
          });
 
        }
 
      }
 
    }
 
  }
 
}
