import { NgModule } from '@angular/core';
import { ContentDrawer } from './content-drawer/content-drawer';
import { Spinner } from './spinner/spinner';
@NgModule({
	declarations: [ContentDrawer,
    Spinner],
	imports: [],
	exports: [ContentDrawer,
    Spinner]
})
export class ComponentsModule {}
