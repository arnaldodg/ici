import { 
  Component, 
  Input 
} from '@angular/core';

@Component({
  selector: 'spinner',
  templateUrl: 'spinner.html'
})
export class Spinner {

  @Input('text') text: string;

  constructor() {
    //console.log('Hello SpinnerComponent Component');
    
  }

}
