import { Injectable } from '@angular/core';
import { Effect, Actions } from '@ngrx/effects';

import { Store, INIT } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';

//import { Storage } from '@ionic/storage';

import * as fromRoot from '../reducers';
import * as ProposalActions from '../actions/proposal.actions';
import * as ClientActions from '../actions/client.actions';
import * as EstateActions from '../actions/estate.actions';
import * as CommonActions from '../actions/common.actions';

export type Action = ProposalActions.All;

import { ApiDataService } from '../providers/api-data.service';

@Injectable()
export class ProposalEffects {

  constructor(
    private actions$: Actions,
    //private local: Storage,
    private store: Store<fromRoot.State>,
    private api: ApiDataService
  ) {

    //console.log('FX::ktor');
  }
  
  @Effect()
  onLoad: Observable<Action> = this.actions$
  .ofType(ProposalActions.LOAD)
  .map(action => action)
  .switchMap((action) => { 
    return this.api.getOpParams() 
  })
  .map((payload) => new ProposalActions.LoadSuccess(payload))
  .do(() => {
    console.log('Proposal.LOAD!');
  })

  @Effect({ dispatch: false })
  onLoadSuccess: Observable<Action> = this.actions$
  .ofType(ProposalActions.LOAD_SUCCESS)
  .map(action => action)
  .switchMap(action => { return Observable.of(null) })
  .do(() => {
    console.log('LOAD:SUCCESS');
    //this.ProposalService.activateStorage();
  });
  
/*  
  @Effect()
  onInit = this.actions$.ofType(INIT)
  .startWith({type: INIT})
  .switchMap((action) => this.api.getFormParams()
    .map(loadedState => {
      console.log('loadedState!');
      return new ProposalActions.LoadSuccess({state: loadedState});
    })
    .catch(err => Observable.of(new ProposalActions.LoadFail( 
      { error: err.message } )) 
    )
  )
*/

}