import { Injectable } from '@angular/core';
import { Effect, Actions } from '@ngrx/effects';

import { Store, INIT } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';

//import { Storage } from '@ionic/storage';

import * as fromRoot from '../reducers';
import * as opParamsActions from '../actions/op-params.actions';
export type Action = opParamsActions.All;

import { ApiDataService } from '../providers/api-data.service';

@Injectable()
export class OpParamsEffects {

  store$: Observable<any>;

  constructor(
    private actions$: Actions,
    //private local: Storage,
    private store: Store<fromRoot.State>,
    private api: ApiDataService
  ) {
    //this.store$ = this.store.select(fromRoot.getOpParamsState);
    //console.log('FX::ktor');
  }

  @Effect()
  onLoad: Observable<Action> = this.actions$
  .ofType(opParamsActions.LOAD)
  .map(action => action)
  .switchMap( (action) => this.api.getOpParams()
    .map( (payload) =>  new opParamsActions.LoadSuccess(payload) )
    .catch( err =>  
      Observable.of( new opParamsActions.LoadFail({error: err.message}) )
    )
  );
  
  @Effect({ dispatch: false })
  onLoadSuccess: Observable<Action> = this.actions$
  .ofType(opParamsActions.LOAD_SUCCESS)
  //.map(action => action)
  .switchMap(action => { return Observable.of(null) })
  .do(() => {
    console.log('LOAD:SUCCESS');
    //this.opParamsService.activateStorage();
  });

  @Effect({ dispatch: false })
  onLoadFail: Observable<Action> = this.actions$
  .ofType(opParamsActions.LOAD_FAIL)
  .map(action => action)
  .switchMap(action => { return Observable.of(null) })
  .do(() => {
    console.log('LOAD:FAIL');
    //this.opParamsService.activateStorage();
  });

/*  
  @Effect()
  onInit = this.actions$.ofType(INIT)
  .startWith({type: INIT})
  .switchMap((action) => this.api.getFormParams()
    .map(loadedState => {
      console.log('loadedState!');
      return new opParamsActions.LoadSuccess({state: loadedState});
    })
    .catch(err => Observable.of(new opParamsActions.LoadFail( 
      { error: err.message } )) 
    )
  )
*/

}