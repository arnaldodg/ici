import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { NativeStorage } from '@ionic-native/native-storage';

import { OperationsPage } from '../pages/operations/operations';
import { PropostasPage } from '../pages/propostas/propostas';
import { StartPage } from '../pages/start/start';
import { RegistrationPage } from '../pages/registration/registration';

import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/observable';
import * as fromRoot from '../reducers';

@Component({
  templateUrl: 'app.html'
})


export class MyApp {
  static readonly STATE_KEY: string  = 'state';

  state: Observable<any>;
  
  rootPage:any = StartPage;
  
  constructor(
    platform: Platform, 
    statusBar: StatusBar, 
    splashScreen: SplashScreen,
    local: NativeStorage,
    private store: Store<fromRoot.State>
  ) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();

      this.store.subscribe((data) => {
        console.log('STATE.CHG!', data);
        //local.setItem(STATE_KEY, data);
      });
    });
  }
}

