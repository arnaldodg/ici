import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { NativeStorage } from '@ionic-native/native-storage';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { HttpClientModule, HttpClient } from '@angular/common/http';
//import { ReactiveFormsModule } from "@angular/forms";

// RXJS imports
import './app.rxjs.operators';

// ngrx
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';

// StoreDevTools
import { StoreDevtoolsModule } from '@ngrx/store-devtools';

// reducer Module
import { reducers } from '../reducers';

// FXs
import { OpParamsEffects } from '../effects/op-params.effects';
import { ProposalEffects } from '../effects/proposal.effects';

// Components
import { ContentDrawer } from '../components/content-drawer/content-drawer';
import { Spinner } from '../components/spinner/spinner';

// App n Pages
import { MyApp } from './app.component';
import { OperationsPage } from '../pages/operations/operations';
import { PropostasPage } from '../pages/propostas/propostas';
import { SettingsPage } from '../pages/settings/settings';
import { TosPage } from '../pages/tos/tos';
import { StartPage } from '../pages/start/start';
import { RegistrationPage } from '../pages/registration/registration';
import { SigninPage } from '../pages/signin/signin';
import { DonePage } from '../pages/done/done';
import { PhoneTokenPage } from '../pages/phone-token/phone-token';
import { ActivationPage } from '../pages/activation/activation';
import { PropostaOptionsPage } from '../pages/proposta-options/proposta-options';

// Providers
import { ApiDataService } from '../providers/api-data.service';


@NgModule({
  declarations: [
    MyApp,
    ContentDrawer,
    Spinner,
    OperationsPage,
    PropostasPage,
    SettingsPage,
    TosPage,
    StartPage,
    RegistrationPage,
    SigninPage,
    DonePage,
    PhoneTokenPage,
    ActivationPage,
    PropostaOptionsPage
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    //ReactiveFormsModule,
    IonicModule.forRoot(MyApp, {
      scrollPadding: false,
      scrollAssist: true,
      autoFocusAssist: false
    }),
    EffectsModule.forRoot([
      OpParamsEffects,
      ProposalEffects
    ]),
    StoreModule.forRoot(reducers),
    StoreDevtoolsModule.instrument({ //instrumentOnlyWithExtension({
      maxAge: 25
    })
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    OperationsPage,
    PropostasPage,
    SettingsPage,
    TosPage,
    StartPage,
    RegistrationPage,
    SigninPage,
    DonePage,
    PhoneTokenPage,
    ActivationPage,
    PropostaOptionsPage
  ],
  providers: [
      StatusBar,
      SplashScreen,
      ApiDataService,
      HttpClientModule,
      NativeStorage,
      {provide: ErrorHandler, useClass: IonicErrorHandler},
    ]
})
export class AppModule {}
