import * as utils from '../providers/common.service';

import { Proposal } from '../models/proposal.model';
import { Client, createClient } from '../models/client.model';
import { Estate, createEstate } from '../models/estate.model';
import { Operation } from '../models/operation.model';
import { Expansion } from '../models/expansion.model';

export function buildProposal(source?: Proposal): Proposal {
  // if (source) { override params -> clone when possible }

  let _client = createClient();
  let _clientId = _client.uuid;

  let _estate = createEstate();
  let _estateId = _estate.uuid;

  let _proposal = {
    uuid: utils.createUUID(),
    timestamp: new Date().toISOString(),
    status: 'new',

    // evolution tracking
    submitDate: null,
    analysisDate: null,
    approvalDate: null,
    paymentDate: null,

    clientId: _clientId,
    estateId: _estateId,
    operationId: null,

    client: _client,
    estate: _estate,

    requestedAmount: 100000,
    includeExpenses: false,

    paramsVersion: null

  }

  _client.proposals.push(_proposal.uuid);
  _client.estates.push(_estate.uuid);
  _estate.clientId = _clientId;

  return _proposal;
}