import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { Observable } from 'rxjs/Observable';
//import 'rxjs/add/observable/fromPromise';

@Injectable()
export class ApiDataService {

  //private opURL = 'https://hml-services.bancointer.com.br/api/inter-ci-configuration-service/v1/simulatorSetup';
  //private opURL = './assets/files/operationData.json';
  private opURL = './assets/files/operacaoDataTest11.json';
  private cnpjURL = 'https://www.receitaws.com.br/v1/cnpj/';
  private cepURL = 'https://viacep.com.br/ws/';

  constructor(
    public http: HttpClient
  ) {
    console.log('Hello ApiDataProvider Provider');
  }

  getOpParams(): Observable<Object>{
    return this.http.get(this.opURL);
  }

  checkCNPJ(cnpj: string): Observable<Object>{
    console.log('api::checkCNPJ', cnpj);
    return this.http.get(this.cnpjURL + cnpj);
  }

  checkCEP(cep: string): Observable<Object>{
    return this.http.get(this.cepURL + cep + '/json');
  }


}
