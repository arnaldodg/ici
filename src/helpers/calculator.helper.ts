import { addMonths } from 'date-fns';
import { Proposal } from '../models/proposal.model';

import { 
  Operation, 
  OperationRules 
} from '../models/operation.model';

interface calcParams {
  tipo?: 'SAC' | 'PRICE';
  valorImovel?: number;     // 150000
  valorSolicitado?: number; // 50000
  taxaMensal?:number;       // 1.9
  qtdParcelas?: number;     // 24
  dataInicial: Date;        // today + 1m
  iof?: boolean;            // false
  itbi?:boolean;            //false 
  seguro?:boolean;          //true
}

export class Calculator { //SAC only - no IOF or at.else

  params: calcParams;

  constructor (params: calcParams) {
    this.params = params;
  }

  execute(params?: calcParams): any {
    if (typeof(params)!=='undefined' || params!==null) this.params = params;

    let _result: any = {};

    let tipo = this.params.tipo;

    let prazo = this.params.qtdParcelas;
    let valorSolicitado = this.params.valorSolicitado;
    let valorImovel = this.params.valorImovel;
    let taxaMensal = this.params.taxaMensal / 100.0;

    let dataInicial = this.params.dataInicial || new Date();
    let dataFinal: Date;

    let txMIP = 0.0050;
    let txDFI = 0.0025;

    let amort = valorSolicitado / prazo;
    
    let valorDFI = valorImovel * txDFI;
    let valorMIP: number;
    let custoSeguros: number;

    let valorIOF: number = 0.0;

    let akku: number = 0.0;
    let akkuFull: number = 0.0;
    
    let saldo: number;
    let jurosSaldo: number;
    let parcela: number;
    let parcelaFull: number;
    let dataParcela: Date;

    if (tipo==='SAC'){
      for (let n = 1; n < prazo+1; n++) {
        saldo = valorSolicitado - ((n-1)*amort);
        jurosSaldo = saldo * taxaMensal;
        parcela = amort + jurosSaldo;
        
        valorMIP = saldo * txMIP;
    
        custoSeguros = valorMIP + valorDFI;
        parcelaFull = parcela + custoSeguros;
        
        akku += parcela;
        akkuFull += parcelaFull;        
        
        _result[String(n)] = {
          num: n,
          parcela: parcela,
          juros: jurosSaldo,
          amortizacao: amort,
          saldo: saldo,
          vencimento: dataParcela,
          mip: valorMIP,
          dfi: valorDFI,
          iof: valorIOF
        }

        _result["summary"] = {
          valorSolicitado: valorSolicitado,
          valorRecebido: valorSolicitado,
          valorSeguro: custoSeguros,
          dataInicial: dataInicial,
          dataFinal: dataFinal
        }

      }
    }

    return _result;
  }
}