import * as _ from 'lodash';

import { Storage } from '@ionic/storage';

import { Loadable } from '../models/loadable.model';
import { Client } from '../models/client.model';
import { Estate } from '../models/estate.model';
import { Proposal } from '../models/proposal.model';

import * as Builders from '../builders/proposal.builder';

import * as ProposalActions from '../actions/proposal.actions';

import * as utils from '../providers/common.service';
import { createSelector } from '@ngrx/store';

export type Action = ProposalActions.All;

export interface State extends Loadable{

  empty: boolean;

  byId: { [key: string]: Proposal };

  allIds: Array<string>;
  
  selected: Proposal;
  selectedId:  string;
}

const defaultState: State = {
  loaded: false,
  loading: false,
  hasError: false,
  error: null,

  empty: true,

  byId: mockItems(),
  
  allIds: _.keys(mockItems()),
  
  selected: null,
  selectedId: null
};

export function mockItems(){
  return {
    'p1': {
      uuid: 'p1',
      timestamp: new Date().toISOString(),
      status: 'sim',

      submitDate: new Date(),
      analysisDate: null,
      approvalDate: null,
      paymentDate: null,

      clientId: 'c1',
      estateId: 'e1',
      operationId: null,

      requestedAmount: 150000,
      includeExpenses: false,

      paramsVersion: null

    },
    'p2': {
      uuid: 'p2',
      timestamp: new Date().toISOString(),
      status: 'sim',

      submitDate: new Date(),
      analysisDate: new Date(),
      approvalDate: null,
      paymentDate: null,

      clientId: 'c2',
      estateId: 'e2',
      operationId: null,

      requestedAmount: 120000,
      includeExpenses: false,

      paramsVersion: null
    }
  }
}

export function initialState(){

  // try localStorage, if empty use defaulState
  //
  return defaultState;
}


//reducer
export function reducer(state: State = initialState(), action: Action): State {
  let _item: Proposal; // proposal (_item)
  let _list: { [key: string]: Proposal }; // proposals (collection)
  
  let _client: Client;
  let _estate: Estate;
  
  switch (action.type){
    case ProposalActions.LOAD:
      console.log('proposal.LOAD');
      return {
        ...state,
        loading: true, 
        loaded: false,
        hasError: false,
        error: ''
      };
      
    case ProposalActions.LOAD_SUCCESS:
      console.log('proposal.LOAD_SUCCESS', action.payload);
      _item = action.payload;
      return {
        ...state, 
        selected: _item,
        loading: false, 
        loaded: true,
        hasError: false,
        error: ''
      }
      
    case ProposalActions.LOAD_FAIL:
    console.log('proposal.LOAD_FAIL');
      
      return {
        ...state,
        loading: false, 
        loaded: true,
        hasError: true,
        error: '' // ?
      }

    case ProposalActions.SAVE:
    console.log('proposal.SAVE');  
      _item = action.payload;
      // update if exists; append if not exists

      return {
        ...state,
        //...action.payload.state, // or else
        loading: true, 
        loaded: false,
        hasError: true,
        error: '' // ?
      }

    case ProposalActions.SELECT:
      console.log('proposal.SELECT', action.payload);

      if (typeof(action.payload)==='string') {
        _item = state.byId[action.payload];
      } else {
        _item = action.payload;
      }

      return {
        ...state, 
        
        //selected: _item,
        selectedId: _item ? _item.uuid : null,
        
        loading: false, 
        loaded: true,
        hasError: false,
        error: ''
      }
    
    case ProposalActions.NEW:
      console.log('proposal.NEW', action.payload);
      let _proposal = action.payload;

      return {
        ...state, 
        byId: {...state.byId, [_proposal.uuid]: _proposal},  
        allIds: [...state.allIds, _proposal.uuid],
        //selected: _proposal,
        selectedId: _proposal.uuid,
        
        loading: false, 
        loaded: true,
        hasError: false,
        error: ''
      }
      
    case ProposalActions.CLONE:
      console.log('proposal.CLONE', action.payload);

      _list = Object.assign({}, state.byId);
      _list[_proposal.uuid] = _proposal;

      return {
        ...state, 
      
        byId: _list,
        selected: Builders.buildProposal(action.payload),
        
        loading: false, 
        loaded: true,
        hasError: false,
        error: ''
      }

    default:
    console.log('proposal.DEFAULT', action);
      return state;
  }
}


// flow control
export const getLoaded = (state: State) => state.loaded;
export const getLoading = (state: State) => state.loading;
export const getHasError = (state: State) => state.hasError;
export const getError = (state: State) => state.error;

//collection
export const getMap = (state: State) => state.byId;
export const getObjects = (state: State) => _.values<Proposal>(state.byId);

// computed selected
export const getSelectedId = (state: State) => state.selectedId;
export const getSelected = (state: State) => state.selectedId ? state.byId[state.selectedId] : null;
