import { Loadable } from '../models/loadable.model';
import { Selector } from '../models/selector.model';
import { Operation } from '../models/operation.model';
import { OpParams } from '../models/op-params.model';

import * as OpFormParamsActions from '../actions/op-params.actions';

import { Storage } from '@ionic/storage';
import { createSelector } from '@ngrx/store';

import * as _ from 'lodash';

export type Action = OpFormParamsActions.All;

const optionsPreset = { // move to DynamoDB as configurable param!
  PROD: 'CREDFLEX',
  DEBC: 'S',
  CORR: 'PÓS-FIXADO',
  PESS: 'PESSOA FÍSICA',
  AMRT: 'SAC'
}

export interface State extends Loadable {
  version?: string;
  
  groupNames: {[key: number]: string};

  selGroups?: { [key: string]: Selector};
  operations?: { [key: string]: Operation };

  globalParams?: any;

  // SelGroup mapped selectors array
  selector: {[key: string]: Array<string>};
  selectorRegex: string;

  // Selected Operation ID
  selectedOpId: string;
  selectedPath: string;
}

const defaultState: State = {
  loaded: false,
  loading: false,
  hasError: false,
  error: '',

  groupNames: {},

  selGroups: {},
  operations: {},

  selector: {},
  selectorRegex: null,

  selectedPath: null,
  selectedOpId: null
};

export function initialState(){
  // try localStorage, if empty use defaulState
  //
  return defaultState;
}


//reducer
export function reducer(state: State = initialState(), action: Action): State {
  let _group, _option: string;

  switch (action.type){
    case OpFormParamsActions.LOAD:
      console.log('opParams::reducer - LOAD');
      return {
        ...state,
        loading: true, 
        loaded: false,
        hasError: false,
        error: ''
      };
      
    case OpFormParamsActions.LOAD_SUCCESS:
      console.log('opParams::reducer - LOAD_SUCCESS', action.payload);
  
      return {
        ...state, 
        ...action.payload,
        loading: false, 
        loaded: true,
        hasError: false,
        error: ''
      }
      
    case OpFormParamsActions.LOAD_FAIL:
    console.log('opParams::reducer - LOAD_FAIL', action.payload);
      
      const _result = {
        ...state,
        loading: false, 
        loaded: true,
        hasError: true,
        error: action.payload.error // ?
      }

      return _result
      
    case OpFormParamsActions.SET_SELECTOR_OPTION:
      _group = action.payload.group;
      _option = action.payload.option;

      return {
        ...state,
        // update selector with 'token' array
        selector: {...state.selector, [_group]: state.selGroups[_group].selectors[_option]},
        loading: false, 
        loaded: true,
        hasError: false,
        error: ''
      }

    case OpFormParamsActions.SELECT_OP:
      return {
        ...state,
        selectedOpId: action.payload,
        loading: false, 
        loaded: true,
        hasError: false,
        error: ''
      }

    default:
      return state;
  }
}

export const getLoaded = (state: State) => state.loaded;
export const getLoading = (state: State) => state.loading;
export const getHasError = (state: State) => state.hasError;
export const getError = (state: State) => state.error;


export const getGroupNames = (state: State) => state.groupNames;
export const getSelGroups = (state: State) => state.selGroups;
export const getOperations = (state: State) => state.operations;

export const getSelectedOpId = (state: State) => state.selectedOpId;
export const getSelectedOp = (state: State) => state.selectedOpId ? state.operations[state.selectedOpId] : null;
export const getSelector = (state: State) => state.selector;

// querys
export const getSelectors = createSelector(getGroupNames, getSelGroups, (_groups, _selGroups) => {
  let _names = _.values(_groups);
  return _names.map( (_key) => { 
    return _selGroups[_key]; 
  });
});


export const getSelectorRegex = createSelector(getSelector, (_selector) => {
  let _groups = Object.keys(_selector);

  return _groups.map((_group) => {
    return `(?=(?:.*?(${_selector[_group].join('|')})))`;
  }).join('') + '(?:.*?(\\d{3}))';

});

