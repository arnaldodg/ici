import { 
  ActionReducer, 
  ActionReducerMap, 
  createSelector,
  combineReducers,
  createFeatureSelector
} from '@ngrx/store';

import * as _ from 'lodash';

/**
 * The compose function is one of our most handy tools. In basic terms, you give
 * it any number of functions and it returns a function. This new function
 * takes a value and chains it through every composed function, returning
 * the output.
 *
 * More: https://drboolean.gitbooks.io/mostly-adequate-guide/content/ch5.html
 */
import { compose } from '@ngrx/core/compose';

/**
 * storeFreeze prevents state from being mutated. When mutation occurs, an
 * exception will be thrown. This is useful during development mode to
 * ensure that none of the reducers accidentally mutates the state.
 */
//import { storeFreeze } from 'ngrx-store-freeze';

/**
 * Every reducer module's default export is the reducer function itself. In
 * addition, each module should export a type or interface that describes
 * the state of the reducer plus any selector functions. The `* as`
 * notation packages up all of the exports into a single object.
 */
//import * as fromSearch from './search';
import * as fromOpParams from './op-params.reducer';
import * as fromProposal from './proposal.reducer';
import * as fromClient from './client.reducer';
import * as fromEstate from './estate.reducer';
import * as fromRegistration from './registration.reducer';

import { Proposal } from '../models/proposal.model';
import { Operation } from '../models/operation.model';

/**
 * As mentioned, we treat each reducer like a table in a database. This means
 * our top level state interface is just a map of keys to inner state types.
 */

export interface State {
  //search: fromSearch.State;
  
  // Registration and session State (or the other way around =])
  registration: fromRegistration.State;

  // TABLES
  //
  // User Proposals (collection, item and actions)
  proposals: fromProposal.State;
  
  // Cliente
  clients: fromClient.State;

  // Imovel
  estates: fromEstate.State;
  
  // Simulator: Operation and Selectors SETUP data + simulation OpSelectors
  opParams: fromOpParams.State;

}


export const reducers: ActionReducerMap<State> = {
  //search: fromSearch.reducer,
  registration: fromRegistration.reducer,
  proposals: fromProposal.reducer,
  clients: fromClient.reducer,
  estates: fromEstate.reducer,
  opParams: fromOpParams.reducer,
  //selectors: fromSelector.reducer
};


// Global State
export const getAppState = (state: State) => state;

// Registration Data
export const getRegistrationState = (state: State) => state.registration;
export const getRegistration = createSelector(getRegistrationState, fromRegistration.getRegistration);
export const getRegistrationPending = createSelector(getRegistrationState, fromRegistration.getIsPending);
export const getRegistrationActivated = createSelector(getRegistrationState, fromRegistration.getIsActivated);
export const getRegistrationCancelled = createSelector(getRegistrationState, fromRegistration.getIsCancelled);
export const getRegistrationDenied = createSelector(getRegistrationState, fromRegistration.getIsDenied);
export const getRegistrationPhoneValidated = createSelector(getRegistrationState, fromRegistration.getPhoneValidated);


// OpFormParams Data
export const getOpParamsState = (state: State) => state.opParams;
export const getOpParamsSelGroups = createSelector(getOpParamsState, fromOpParams.getSelGroups);
export const getOpParamsOperations = createSelector(getOpParamsState, fromOpParams.getOperations);
export const getOpParamsLoading = createSelector(getOpParamsState, fromOpParams.getLoading);

export const getSelectedOpId = createSelector(getOpParamsState, fromOpParams.getSelectedOpId);

export const getSelectedOp = createSelector(
  getSelectedOpId, getOpParamsOperations, 
  (id, operations) => {
    return operations[id];
  });

export const getOpParamsSelectors = createSelector(getOpParamsState, fromOpParams.getSelectors);
export const getOpParamsSelectorRegex = createSelector(getOpParamsState, fromOpParams.getSelectorRegex);

export const getFilteredOperations = createSelector(getOpParamsSelectorRegex, getOpParamsOperations, 
  (_regex, _operations) => {
    let _operationList = _.values(_operations);
    let _exp = new RegExp(_regex, 'g');
    return _operationList.filter(function(_item){ return _item.path.match(_exp)!==null });
  });

// Proposal Data
export const getProposalsState = (state: State) => state.proposals;
export const getProposals = createSelector(getProposalsState, fromProposal.getMap); // byId list
export const getProposalList = createSelector(getProposalsState, fromProposal.getObjects); // byId list
export const getSelectedProposalId = createSelector(getProposalsState, fromProposal.getSelectedId);
export const getSelectedProposal = createSelector(
  getProposals, getSelectedProposalId, 
  (_proposals, _id) => {
    console.log('inx.getSelectedProposal', _id);
    return _proposals[_id]
  });

// Client Data
export const getClientsState = (state: State) => state.clients;
export const getClients = createSelector(getClientsState, fromClient.getMap); // byId list
export const getClientList = createSelector(getClientsState, fromClient.getObjects); // byId list
export const getSelectedClient = createSelector(getClientsState, fromClient.getSelected);

// Estate Data
export const getEstatesState = (state: State) => state.estates;
export const getEstates = createSelector(getEstatesState, fromEstate.getMap); // byId List
export const getEstateList = createSelector(getEstatesState, fromEstate.getObjects); // byId List
export const getSelectedEstate = createSelector(getEstatesState, fromEstate.getSelected);

/**
 * Some selector functions create joins across parts of state. This selector
 * composes the search result IDs to return an array of books in the store.
 */
/*export const getSearchResults = createSelector(getBookEntities, getSearchBookIds, (books, searchIds) => {
  return searchIds.map(id => books[id]);
});
*/

export const getProposalCards = createSelector(getProposalList, getClients, getEstates, 
  (proposals, clients, estates ) => {
    return proposals.map((proposal) => {
      return {
        ...proposal, 
        client: clients[proposal.clientId],
        estate: estates[proposal.estateId],
        operation: null
      };
    });
  });

export const getCurrentProposal = createSelector(
  getSelectedProposal, getSelectedClient, getSelectedEstate, getSelectedOp,
  (_proposal, _client, _estate, _operation) => {
    console.log('>>selectedProposal', _proposal, _operation);
    let _result = Object.assign({}, _proposal, {client: _client, estate: _estate, operation: _operation}) || {};
    console.log('getCurrProp!CHG', _result);
    return _result;
  });


export const getOperationItems = createSelector(getOpParamsOperations, 
  (_operations) => {
    return _.values(_operations);
  }); 