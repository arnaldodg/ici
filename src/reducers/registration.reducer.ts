import * as RegistrationActions from '../actions/registration.actions';
import { Registration } from '../models/registration.model';

import { Storage } from '@ionic/storage';

export type Action = RegistrationActions.All;

export interface State {
  loaded?: boolean;
  loading?: boolean;
  hasError?: boolean;
  error?: string;
  
  registration: Registration;
}

const defaultState: State = {
  loaded: false,
  loading: false,
  hasError: false,
  error: '',

  registration: {
    pending: false,
    activated: false,
    cancelled: false,
    denied: false,

    phoneValidated: false,

    userName: '',
    displayName: ''
  }
};

export function initialState(){

  // try localStorage, if empty use defaulState
  //
  return defaultState;
}


//reducer
export function reducer(state: State = initialState(), action: Action): State {
  
  switch (action.type){
    case RegistrationActions.REQUEST:
      console.log('registration::reducer - REQUEST');
      var oldReg = state.registration;
      return {
        ...state,
        loading: true, 
        loaded: false,
        hasError: false,
        error: '',
        registration: {...oldReg, pending: true}
      };
      
    case RegistrationActions.REQUEST_SUCCESS:
      console.log('registration::reducer - REQUEST_SUCCESS', action.payload);
      var oldReg = state.registration;
      return {
        ...state, 
        ...action.payload,
        loading: false, 
        loaded: true,
        hasError: false,
        error: '',
        registration: {...oldReg, pending: true}
      }
      
    case RegistrationActions.REQUEST_FAIL:
    console.log('registration::reducer - REQUEST_FAIL');
      
      const _result = {
        ...state,
        //...action.payload.state, // or else
        loading: false, 
        loaded: true,
        hasError: true,
        error: '' // ?
      }

      return _result
    
    case RegistrationActions.CANCEL:
      var oldReg = state.registration;
      return {
        ...state, 
        //...action.payload,
        loading: false, 
        loaded: true,
        hasError: false,
        error: '',
        registration: {...oldReg, pending: false}
      }
      
    default:
      return state;
  }

}

// Stream state
export const getLoaded = (state: State) => state.loaded;
export const getLoading = (state: State) => state.loading;
export const getHasError = (state: State) => state.hasError;
export const getError = (state: State) => state.error;

// RegRequest status
export const getRegistration = (state: State) => state.registration;
export const getIsPending = (state: State) => state.registration.pending;
export const getIsActivated = (state: State) => state.registration.activated;
export const getIsCancelled = (state: State) => state.registration.cancelled;
export const getIsDenied = (state: State) => state.registration.denied;
export const getPhoneValidated = (state: State) => state.registration.phoneValidated;

// User Data
export const getUserName = (state: State) => state.registration.userName;
export const getDisplayName = (state: State) => state.registration.displayName;
