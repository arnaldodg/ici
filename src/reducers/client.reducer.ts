import { Loadable } from '../models/loadable.model';
import { Client } from '../models/client.model';

import * as ClientActions from '../actions/client.actions';
import * as ProposalActions from '../actions/proposal.actions';

import * as _ from 'lodash';
import * as DateFns from 'date-fns';

export interface State extends Loadable {
  
  byId: { [key: string]: Client };
  allIds: Array<string>;
  selectedId: string;
  selected: Client;
}

function mockItems(){
  return {
    'c1': {
      uuid: 'c1',
      status: 'sim',
      nome: 'Clodoado Noronha',
      cpfpj: '123456789-00',
      renda: null,
      nascimento: new Date().toISOString(),
      sexo: 'M',
      estadoCivil: 'C',

      estates: [],
      proposals: []
    },
    'c2': {
      uuid: 'c2',
      status: 'sim',
      nome: 'Rafaela Ferreira',
      cpfpj: '123456789-00',
      renda: null,
      nascimento: new Date().toISOString(),
      sexo: 'F',
      estadoCivil: 'S',

      estates: [],
      proposals: []
    }
  }
}

export const defaultState: State = {
  loading: false,
  loaded: false,
  hasError: false,
  error: null,

  byId: mockItems(),
  allIds: _.keys(mockItems()),
  selectedId: null,
  selected: null

}

export function reducer(state: State = defaultState, action){
  
  let _client = null;
  let _proposal = null;
  
  switch (action.type) {

    case ProposalActions.NEW:
      console.log('client.prop.NEW', action);
        
      _proposal = action.payload;
      _client = _proposal.client;

      return {
        ...state,
        byId: {...state.byId, [_client.uuid]: _client},  
        allIds: [...state.allIds, _client.uuid],
        selected: _client,
        selectedId: _client.uuid,
        
        loading: false, 
        loaded: true,
        hasError: false,
        error: ''
      }

      case ProposalActions.SELECT:
      console.log('client.prop.SELECT', action.payload);

      if (typeof(action.payload)==='string') {
        _client = state.byId[action.payload.client.uuid];
      } else {
        _client = action.payload ? action.payload.client : null;
      }

      return {
        ...state, 
        
        //selected: _client,
        selectedId: _client ? _client.uuid : null,
        
        loading: false, 
        loaded: true,
        hasError: false,
        error: ''
      }
    

    case ClientActions.NEW:
      console.log('client.NEW', action);
      
      _client = action.payload;
      return {
        ...state,
        byId: {...state.byId, [_client.uuid]: _client},  
        allIds: [...state.allIds, _client.uuid],
        selected: _client,
        selectedId: _client.uuid,
        
        loading: false, 
        loaded: true,
        hasError: false,
        error: ''
      }

    default:
      console.log('client.DEFAULT', action);
      return state;
    
  }
}

// flow control
export const getLoaded = (state: State) => state.loaded;
export const getLoading = (state: State) => state.loading;
export const getHasError = (state: State) => state.hasError;
export const getError = (state: State) => state.error;

//collection
export const getMap = (state: State) => state.byId;
export const getObjects = (state: State) => _.values<Client>(state.byId);

// selected Item
export const getSelectedId = (state: State) => state.selectedId;
export const getSelected = (state: State) => state.selectedId ? state.byId[state.selectedId] : null;


