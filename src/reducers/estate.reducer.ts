import { Loadable } from '../models/loadable.model';
import { Estate } from '../models/estate.model';

import * as EstateActions from '../actions/estate.actions';
import * as ProposalActions from '../actions/proposal.actions';


import * as _ from 'lodash';

export interface State extends Loadable {

  byId: { [key: string]: Estate };
  allIds: Array<string>;
  selectedId: string;
  selected: Estate;
}

function mockItems(){
  return {
    'e1': {
      uuid: 'e1',
      valor: 150000,
      cep: 30000000,
      uf: 'MG',
      tipo: 'C',
      clientId: 'c1',
      
      // docs
      matricula: {url: '', thumb: ''},
      fotos: []
    },
    'e2': {
      uuid: 'e2',
      valor: 220000,
      cep: 30000000,
      uf: 'MG',
      tipo: 'R',
      clientId: 'c2',
      
      // docs
      matricula: {url: '', thumb: ''},
      fotos: []
    }
  }
}

export const defaultState: State = {
  loading: false,
  loaded: false,
  hasError: false,
  error: null,

  byId: mockItems(),
  allIds: _.keys(mockItems()),
  selectedId: null,
  selected: null
}

export function reducer(state: State = defaultState, action){
  let _estate = null;
  let _proposal = null;

  switch (action.type) {
    
    case ProposalActions.NEW:
      console.log('estate.prop.NEW', action);
        
      _proposal = action.payload;
      _estate = _proposal.estate;
      return {
        ...state,
        byId: {...state.byId, [_estate.uuid]: _estate},  
        allIds: [...state.allIds, _estate.uuid],
        selected: _estate,
        selectedId: _estate.uuid,
        
        loading: false, 
        loaded: true,
        hasError: false,
        error: ''
      }

    case ProposalActions.SELECT:
      console.log('estate.prop.SELECT', action.payload);

      if (typeof(action.payload)==='string') {
        _estate = state.byId[action.payload.estate.uuid];
      } else {
        _estate = action.payload ? action.payload.estate : null;
      }

      return {
        ...state, 
        
        //selected: _estate,
        selectedId: _estate ? _estate.uuid : null,
        
        loading: false, 
        loaded: true,
        hasError: false,
        error: ''
      }

    case EstateActions.NEW:
      console.log('estate.NEW', action);
      
      _estate = action.payload;
      return {
        ...state,
        byId: {...state.byId, [_estate.uuid]: _estate},  
        allIds: [...state.allIds, _estate.uuid],
        selected: _estate,
        selectedId: _estate.uuid,
        
        loading: false, 
        loaded: true,
        hasError: false,
        error: ''
      }
  
    default:
      console.log('estate.DEFAULT');
      return state;
    
  }
}

// flow control
export const getLoaded = (state: State) => state.loaded;
export const getLoading = (state: State) => state.loading;
export const getHasError = (state: State) => state.hasError;
export const getError = (state: State) => state.error;

//collection
export const getMap = (state: State) => state.byId;
export const getObjects = (state: State) => _.values<Estate>(state.byId);


// selected Item
export const getSelectedId = (state: State) => state.selectedId;
export const getSelected = (state: State) => state.selectedId ? state.byId[state.selectedId] : null;


