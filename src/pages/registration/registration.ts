import { Component, ViewChild } from '@angular/core';
import { 
  IonicPage,
  NavController,
  NavParams,
  AlertController
} from 'ionic-angular';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { Observable } from 'rxjs/Observable';

import { Store } from '@ngrx/store';
import * as fromRoot from '../../reducers';
import * as registrationActions from '../../actions/registration.actions';

import { ApiDataService } from '../../providers/api-data.service';

import { DonePage } from '../done/done';
import { PhoneTokenPage } from '../phone-token/phone-token';

@IonicPage()

@Component({
  selector: 'page-registration',
  templateUrl: 'registration.html',
})
export class RegistrationPage {
  loading: boolean = false; //Observable<boolean>;

  labelNext: string = "Próx";

  @ViewChild('defaultFocus') defaultFocus: any;

  @ViewChild('signupSlider') signupSlider: any;
  slideOneForm: FormGroup;
  slideTwoForm: FormGroup;
 
  submitAttempt: boolean = false;

  cnpj: string;
  fone: string = '';
 
  constructor(
    public navCtrl: NavController, 
    public formBuilder: FormBuilder,
    private navParams: NavParams,
    private alertCtrl: AlertController,
    
    private api: ApiDataService,
    private store: Store<fromRoot.State>
  ) {

    // Store Subs
    //this.loading = this.store.select(fromRoot.)
    
    this.cnpj = this.navParams.get('cnpj');
    this.slideOneForm = this.formBuilder.group({
      cnpj: [
        this.cnpj, 
        Validators.compose([
          Validators.maxLength(30), 
          Validators.pattern('[a-zA-Z ]*'), 
          Validators.required
        ])
      ],
      razao: ['',
        Validators.compose([
          Validators.maxLength(50), 
          Validators.pattern('[a-zA-Z ]*'), 
          Validators.required
        ])
      ],
      logradouro: ['',
        Validators.compose([
          Validators.maxLength(50), 
          Validators.pattern('[a-zA-Z ]*'), 
          Validators.required
        ])
      ],
      numero: [''],
      complemento: [''],
      bairro: ['',
        Validators.compose([
          Validators.maxLength(30), 
          Validators.pattern('[a-zA-Z ]*'), 
          Validators.required
        ])
      ],
      uf: ['',
        Validators.compose([
          Validators.maxLength(30), 
          Validators.pattern('[a-zA-Z ]*'), 
          Validators.required
        ])
      ],
      municipio: ['',
        Validators.compose([
          Validators.maxLength(40), 
          Validators.pattern('[a-zA-Z ]*'), 
          Validators.required
        ])
      ],
      cep: ['',
        Validators.compose([
          Validators.maxLength(10), 
          Validators.pattern('[a-zA-Z ]*'), 
          Validators.required
        ])
      ],

      nomeRepresentante: [''],
      cpfRepresentante: [''],
      emailRepresentante: [''],
      emailConfRepresentante: [''],
      telRepresentante: ['']
    });

    this.api.checkCNPJ(this.cnpj).do(() => this.loading = true ).subscribe((data) => {
      console.log('chkCNPJ', data);
      this.slideOneForm.patchValue({
        cnpj: data['cnpj'],
        razao: data['nome'],
        logradouro: data['logradouro'],
        numero: data['numero'],
        complemento: data['complemento'],
        bairro: data['bairro'],
        uf: data['uf'],
        municipio: data['municipio'],
        cep: data['cep']
      }); 

      // look n feel
      setTimeout(()=> {this.loading = false;}, 350);
    });

    

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RegistrationPage');

    // setFocus on Razao Social!!!
    setTimeout(() => {
      if (this.signupSlider.realIndex==0) this.defaultFocus.setFocus();
    }, 750);

    // define onchange CEP listener w debounce when 8 digits fills


  }


  isFirstSlide(): boolean{
    return this.signupSlider.realIndex!=0;
  }
  
  isLastSlide(): boolean{
    return this.signupSlider.isEnd();
  }
  
  isTosSlide():boolean{
    return this.signupSlider.realIndex==2;
  }

  next(){
    if (this.isLastSlide()) {
      this.store.dispatch(new registrationActions.Request());
      this.navCtrl.setRoot(DonePage);
    }

    this.signupSlider.slideNext();
  }
  
  prev(){
    if (this.signupSlider.isBeginning()) this.navCtrl.pop();
    
    this.signupSlider.slidePrev();
  }

  slideChanged(){
    let currentIndex = this.signupSlider.getActiveIndex();
    console.log('Current index is', currentIndex);
    
    this.labelNext = this.signupSlider.isEnd() ? "Aceitar" : "Próx";
  }

  sendMail(){

  }

  checkPhone(evt){
    let alert = this.alertCtrl.create({
      title: 'Validar Telefone',
      subTitle: 'Informe o token recebido por SMS',
      inputs: [{
        name: 'token',
        placeholder: '....'
      }],
      buttons: [{
        text: 'Cancelar',
        role: 'cancel',
        handler: () => {}
      },{
        text: 'Ok',
        handler: (data) => { console.log('token', data)}
      }]
    });
    alert.present();
  }

}
