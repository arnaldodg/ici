import {
  Component,
  ViewChild
} from '@angular/core';

import { 
  NavController,
  NavParams,
  ViewController,
  Navbar
} from 'ionic-angular';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { Store, Selector } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';

import * as fromRoot from '../../reducers';

import { Proposal } from '../../models/proposal.model';
import { OpParams } from '../../models/op-params.model';
import { Operation, OperationRules } from '../../models/operation.model';
import { Selector as OpSelector } from '../../models/selector.model';

import * as OpParamsActions from '../../actions/op-params.actions';
import * as ProposalActions from '../../actions/proposal.actions';
import { PropostasPage } from '../propostas/propostas';


@Component({
  selector: 'page-operations',
  templateUrl: 'operations.html'
})

export class OperationsPage {

  // CONSTs
  private readonly slideTitles = [
    'Cliente',
    'Imóvel',
    'Operação',
    'Prazo',
    'Prazo' // glitch workaround!?
  ];

  // DOM
  @ViewChild('navBar') navBar: Navbar;
  @ViewChild('slides') slides: any;
  @ViewChild('operList') content: any;

  private slideTitle = this.slideTitles[0];

  operationParams: any = {};

  parcelas: number = 48;

  pessoaForm: FormGroup;
  imovelForm: FormGroup;

  drawerOptions: any;

  slideIndex: any = "2";

  firstLoad: boolean = true;
  loading$: Observable<boolean>;
  opParams$: Observable<any>;
  preventSend$: Observable<boolean> = Observable.of(true);
  incluirDespesas: boolean = true;

  controlKeys: any;
  controls: any;
  operKeys: any;
  operations: {};

  selectorControls: {};
  selectionChain: {};

  selectors$: Observable<Array<OpSelector>>;
  selectorRegEx$: Observable<string>;

  state: any = {
    PROD: 'CRE',
    DEBC: 'S',
    CORR: 'POS',
    PESS: 'PF',
    AMRT: 'SAC'
  }

  selectedOpId: number;
  selectedOp: Operation;
  isSelected: boolean = false;
  selectedOp$: Observable<Operation>; // rdx refactor

  operations$: Observable<Operation[]>;

  proposal: Proposal = null;
  proposal$: Observable<any>;

  constructor(
    public navCtrl: NavController,
    private navParams: NavParams,
    private viewCtrl: ViewController,

    private formBuilder: FormBuilder,
    
    private store: Store<fromRoot.State>,
    
  ) {

    this.proposal = this.navParams.get('prop');
    //let hasData = !(this.proposal===null || typeof(this.proposal)==='undefined');
    
    // bring selected Proposal from Store
    this.pessoaForm = this.formBuilder.group({
      nome: [
        '',
        Validators.compose([
          Validators.maxLength(30),
          Validators.pattern('[a-zA-Z ]*'),
          Validators.required
        ])
      ],
      cpf: [
        '',
        Validators.compose([
          Validators.maxLength(11),
          Validators.pattern('[a-zA-Z ]*'),
          Validators.required
        ])
      ],
      estadoCivil: [
        '',
        Validators.compose([
          Validators.maxLength(50),
          Validators.pattern('[a-zA-Z ]*'),
          Validators.required
        ])
      ],
      sexo: [
        ''
      ],
      nascimento: [
        ''
      ],
      renda: [
        ,
        Validators.compose([
          Validators.pattern(/^[0-9]*\.[0-9]{2}$ or ^[0-9]*\.[0-9][0-9]$/),
          Validators.required
        ])
      ]
    });
    
    this.imovelForm = this.formBuilder.group({
      valorImovel: [
        '',
        Validators.compose([
          Validators.maxLength(12),
          Validators.required
        ])
      ],
      cepImovel: [
        '',
        Validators.compose([
          Validators.maxLength(11),
          Validators.pattern('[a-zA-Z ]*'),
          Validators.required
        ])
      ],
      naturezaImovel: [
        '',
        Validators.compose([
          Validators.maxLength(50),
          Validators.pattern('[a-zA-Z ]*'),
          Validators.required
        ])
      ]
    });
    
    // reset Drawer
    this.drawerOptions = {
      handleHeight: 50,
      thresholdFromBottom: 180,
      thresholdFromTop: 200,
      bounceBack: true
    }
    
    // quick gambi: selector class
    this.selectorControls = Object.assign({});
    this.selectionChain = Object.assign({});
    
    // Assign opParams observable
    this.loading$ = this.store.select(fromRoot.getOpParamsLoading);
    
    this.proposal$ = this.store.select(fromRoot.getCurrentProposal);
    this.proposal$.subscribe((_prop) => {
      console.log('_prop!', _prop);
      if(typeof(_prop)!=='undefined' && _prop!==null) {
        _prop.client ? this.pessoaForm.patchValue(_prop.client) : null;
        _prop.estate ? this.imovelForm.patchValue(_prop.estate) : null;
      }
    });
    
    this.selectedOp$ = this.store.select(fromRoot.getSelectedOp);
    //this.selectedOp$.subscribe(_data => console.log('>> selectedOp', _data));
    
    // Subscribe for now -> should be an async template
    this.selectors$ = this.store.select(fromRoot.getOpParamsSelectors);
    this.selectors$.subscribe((_selectors) => {

      console.log('selectors', _selectors);

      // Get controls structs
      this.controls = _selectors;

      // setup Selectors Maps
      this.controls.map(_control => {
        let _id = _control.id;
        this.selectorControls[_id] = _control.selectors;
        this.selectionChain[_id] = [];
      });

    });

    this.selectorRegEx$ = this.store.select(fromRoot.getOpParamsSelectorRegex);
    this.selectorRegEx$.subscribe(_data => console.log('regex:', _data));

    this.operations$ = this.store.select(fromRoot.getFilteredOperations); // getOperationItems);

    this.selectedOp$ = this.store.select(fromRoot.getSelectedOp);
    //this.selectedOp$.subscribe(_op => _op ? _op.id : 0);
  }

  ionViewDidLoad(){
    this.viewCtrl.setBackButtonText('Fechar');
    
  }

  ionViewWillLeave() {
    console.log("Looks like I'm about to leave :(");
    
    // bogus payload
    this.store.dispatch(new ProposalActions.Save({a: 1, b: 2, c: 3}));

    this.store.dispatch(new ProposalActions.Select(null));
    
    this.store.dispatch(new OpParamsActions.SelectOp(null));


  }

  onSelectorClick(evt) {
    
    //console.log('evt', evt);

    // Unselect anyway =]
    //this.store.dispatch(new OpParamsActions.SelectOp(null));

    var senderId = null;
    if (evt.srcElement || evt.currentTarget) {
      senderId = evt.srcElement.attributes.id || evt.currentTarget.id;
    } else if (evt._elementRef) {
      senderId = evt._elementRef.nativeElement.id;
    }

    var _componentName = evt._componentName
    //console.log('selClick', senderId, evt._componentName, evt.value);

    var _value = evt.value;
    var result;
    switch (_componentName) {
      case 'segment':
        result = _value;
        break;

      case 'toggle':
        result = _value ? 'S' : 'N'
        break;
      default: // "button"
        result = _value;
      // done
    }

    console.log('selected Value is:', result); // Control Selected Value

    // DISPATCH
    this.store.dispatch(new OpParamsActions.SetSelectorOption({group: senderId, option: result}));
  }

  onOperationClick(evt) {
    console.log('selectedOp', this.selectedOp, evt);

    var _elId = evt.srcElement.parentElement.parentElement.getAttribute("id");
    this.selectedOp = evt.srcElement.parentElement.parentElement;

    console.log('operationClk', evt);
    console.log('id?', _elId);
    var _opIdMatch = _elId.match(/\b[0-9]{3}\b/)
    var _opId = (_opIdMatch.length===1) ? Number(_opIdMatch[0]) : 0;
    this.selectedOpId = _opId;
    
    console.log('selParams', this.operationParams);
    
    this.store.dispatch(new OpParamsActions.SelectOp(this.selectedOpId));
  }

  onSlideChange() {
    let currentIndex = this.slides.getActiveIndex();
    console.log('Current index is', currentIndex);

    if (currentIndex == 2 && this.firstLoad) {
      //dispatch Load action!
      this.store.dispatch(new OpParamsActions.Load());
      this.firstLoad = false;
    }
    this.slideTitle = this.slideTitles[currentIndex];

    this.slideIndex = String(currentIndex);
  }
  
  onSegmentSelect(evt){
    console.log('segment', evt, evt._value);
    let _value = evt._value;
    this.slides.slideTo(Number(_value), 500);
  }

  onCloseClick(evt){
    this.navCtrl.setRoot(PropostasPage);
  }

}
