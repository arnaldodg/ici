import { 
  Component 
} from '@angular/core';

import { 
  NavController,
  PopoverController,
  ActionSheetController
} from 'ionic-angular';

import * as _ from 'lodash';

import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';

import * as fromRoot from '../../reducers';
import { Proposal } from '../../models/proposal.model';
import { Client } from '../../models/client.model';
import { Estate } from '../../models/estate.model';

import * as ProposalActions from '../../actions/proposal.actions';

import * as Builders from '../../builders/proposal.builder';

import { SettingsPage } from '../settings/settings';
import { OperationsPage } from '../operations/operations';
import { PropostaOptionsPage } from '../proposta-options/proposta-options';

@Component({
  selector: 'page-propostas',
  templateUrl: 'propostas.html'
})
export class PropostasPage {
  settingsPage: any = SettingsPage;
  operationsPage: any = OperationsPage;

  private searchToken: string;
  private showSearch: boolean = false;

  proposals$: Observable<any>;
  prop$: Observable<any>;

  constructor(
    public navCtrl: NavController,
    private popoverCtrl: PopoverController,
    private actionSheetCtrl: ActionSheetController,
    private store: Store<fromRoot.State>,
  
  ) {
    this.proposals$ = this.store.select(fromRoot.getProposalList);
    
    this.prop$ = this.store.select(fromRoot.getProposalCards);

  }

  onSearchClick(evt){
    this.showSearch = !this.showSearch;
  }

  onSearchChange(evt){

  }

  onSearchCancel(evt){
    this.showSearch = false;
  }

  doRefresh(refresher){
    setTimeout(function(){ refresher.complete()}, 750);
  }

  swipe(evt, prop){
    console.log('swipe', evt);
  }

  expand(evt, prop){
    console.log('expand', evt);
  }

  presentPopover(evt, prop){
    /*let popover = this.popoverCtrl.create(PropostaOptionsPage);
    popover.present({
      ev: evt
    });*/

    let actionSheet = this.actionSheetCtrl.create({
      title: 'Opções',
      buttons: [
        {
          text: 'Compartilhar',
          //role: 'destructive',
          handler: () => {
            console.log('Compartilhar clicked');
          }
        },
        {
          text: 'Clonar',
          //role: 'destructive',
          handler: () => {
            console.log('Clonar clicked');
          }
        },
        {
          text: 'Arquivar',
          handler: () => {
            console.log('Arquivar clicked');
          }
        },
        {
          text: 'Excluir',
          role: 'destructive',
          handler: () => {
            console.log('Excluir clicked');
          }
        },
        {
          text: 'Cancelar',
          role: 'cancelar',
          handler: () => {
            console.log('Cancelar clicked');
          }
        }
      ]
    });
 
    actionSheet.present();
  }

  isCurrentStage(value){
    return (typeof(value)==='undefined' || value===null) ? 'stage' : 'current-stage';
  }

  open(prop){
    console.log('target', prop);

    // select proposal
    this.store.dispatch(new ProposalActions.Select(prop));
    
    this.navCtrl.setRoot(OperationsPage, {prop: prop});
  }
  
  onNewClick(evt){  
    let _proposal = Builders.buildProposal();
    console.log('new Prop', _proposal);

    this.store.dispatch(new ProposalActions.New(_proposal));
    this.navCtrl.setRoot(OperationsPage, {prop: null});
  }
}
