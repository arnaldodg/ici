import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PhoneTokenPage } from './phone-token';

@NgModule({
  declarations: [
    PhoneTokenPage,
  ],
  imports: [
    IonicPageModule.forChild(PhoneTokenPage),
  ],
})
export class PhoneTokenPageModule {}
