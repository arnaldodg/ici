import { Component, ViewChild } from '@angular/core';
import { 
  IonicPage, 
  NavController, 
  NavParams,
  ViewController 
} from 'ionic-angular';
import { PropostasPage } from '../propostas/propostas';

@IonicPage()
@Component({
  selector: 'page-signin',
  templateUrl: 'signin.html',
})
export class SigninPage {

  @ViewChild('defaultFocus') defaultFocus: any;

  userName: string = '';
  userPwd: string = '';

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    private viewCtrl: ViewController
  ) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SigninPage');
    // setFocus on Razao Social!!!
    setTimeout(() => {
      this.defaultFocus.setFocus();
    }, 500);
  }

  dismiss() {
    let data = { 'foo': 'bar' };
    this.viewCtrl.dismiss(data);
  }

  send(evt){
    setTimeout(() => this.navCtrl.setRoot(PropostasPage), 300);
  }

}
