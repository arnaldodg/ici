import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PropostaOptionsPage } from './proposta-options';

@NgModule({
  declarations: [
    PropostaOptionsPage,
  ],
  imports: [
    IonicPageModule.forChild(PropostaOptionsPage),
  ],
})
export class PropostaOptionsPageModule {}
