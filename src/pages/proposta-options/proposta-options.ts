import { Component } from '@angular/core';
import { 
  IonicPage,
  NavController,
  NavParams,
  ViewController
} from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-proposta-options',
  templateUrl: 'proposta-options.html',
})
export class PropostaOptionsPage {

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    private viewCtrl: ViewController
  ) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PropostaOptionsPage');
  }

  close(){
    this.viewCtrl.dismiss();
  }

}
