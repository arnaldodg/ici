import { Component } from '@angular/core';
import { 
  IonicPage, 
  NavController, 
  NavParams 
} from 'ionic-angular';

import { PropostasPage } from '../propostas/propostas';

@IonicPage()
@Component({
  selector: 'page-activation',
  templateUrl: 'activation.html',
})
export class ActivationPage {
  
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ActivationPage');
  }

  startUp(){
    this.navCtrl.setRoot(PropostasPage);
  }

}
