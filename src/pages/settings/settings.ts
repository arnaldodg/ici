import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { TosPage } from '../tos/tos';
@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html',
})
export class SettingsPage {

  tosPage: any = TosPage;
  
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams) {
  }

  ionViewDidLoad() {

  }

}
