import { Component } from '@angular/core';
import { 
  IonicPage, 
  NavController, 
  NavParams,
  AlertController,
  ModalController,
  Modal
} from 'ionic-angular';

import { Validators } from '@angular/forms';

// STATE
import { Store } from '@ngrx/store';
import * as fromRoot from '../../reducers';
import { Registration } from '../../models/registration.model';
import * as RegistrationActions from '../../actions/registration.actions';

import { Observable } from 'rxjs/Observable';

import { RegistrationPage } from "../../pages/registration/registration";
import { SigninPage } from "../../pages/signin/signin";
import { ActivationPage } from '../activation/activation';

@IonicPage()
@Component({
  selector: 'page-start',
  templateUrl: 'start.html',
})
export class StartPage {

  registration$: Observable<Registration>;
  regPending$: Observable<boolean>;
  regActivated$: Observable<boolean>;


  cnpj: string = '18945670000146';
  token: string = '';

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    private alertCtrl: AlertController,
    private modalCtrl: ModalController,
    private store: Store<fromRoot.State>
  ) {

    this.registration$ = this.store.select(fromRoot.getRegistration);
    this.regPending$ = this.store.select(fromRoot.getRegistrationPending);
    this.regActivated$ = this.store.select(fromRoot.getRegistrationActivated);

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad StartPage');
  }

  send(evt){
    console.log('CNPJ', this.cnpj, evt);
    this.navCtrl.push(RegistrationPage, {cnpj: this.cnpj});
  }
  
  cancel(evt){
    // confirm cancel Registration req
    let alert = this.alertCtrl.create({
      title: 'Cancelar pedido de Registro?',
      subTitle: 'Confirme para cancelar o pedido de registro',
      buttons:[{
        text: 'Não',
        role: 'cancel'
        },{
        text: 'Sim',
        handler: () => { 
          console.log('Cancelar Reg!');
          this.store.dispatch(new RegistrationActions.Cancel(this.cnpj));
        }
      }] 
    });

    alert.present();
  }

  validate(evt){
    console.log('token', this.token);

    // mock always OK
    setTimeout(() => this.navCtrl.setRoot(ActivationPage), 300);
  }

  showSignin(){
    let profileModal = this.modalCtrl.create(SigninPage);
    profileModal.onDidDismiss(data => {
      console.log(data);
    });
    profileModal.present();
  }

  
}
