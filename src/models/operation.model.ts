export interface OperationRules{
  imovelMax: number; // Validator
  valorMin: number;  // Validator
  valorMax: number;  // Validator
  prazoMin: number;  // Validator -> slider param
  prazoMax: number;  // Validator -> slider param
  taxaJurosAnualNominal: number;
  valorMinPrestacao: number; // Validator
  idadeMax: number; // Validator Nascimento PF
  idadePrazoMax: number;  // Validator Nascimento PF + prazo
  codSeguradora: number;
  codSeguradoraDFI: number;
  codApolice: number;
  codApoliceDFI: number;
  codSeguradora2: number;
  codSeguradora2DFI: number;
  codApolice2: number;
  codApolice2DFI: number;
  ltv: number; // Validator -> Valor Imóvel vs Valor Solicitado [+ despesas]
  comprometimentoRenda: number; // Validator % Renda vs ValorParcela
  indice: number | null; // ???
  ltvTotalOperacao: boolean; // Validator (ValorSolicitado + despesas) vs LTV
  itbi: boolean; // Calcular ITBI
  iof: boolean;  // Calcular IOF -> PF 3.38% | PJ(SIMPLES) 1,70% | PF(N_SIMPLES) 4,10%
  taxaAdministracao: number; // Tx Administrativa PJ (usualmente)
}

export interface Operation { // OperationBody! -> inject ID?!
  id: string;
  name: string;
  level: number;
  path: string;
  group: string;
  type: string;
  label: string;

  rules: OperationRules;
}
