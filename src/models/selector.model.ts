export interface Selector {
  id: string;
  type: string;  // selector control type (btn-group, list, switch)
  cols: number;
  label: string;
  options: Array<string> // "string keys to selectors values"
  selectors: { 
    [key: string]: Array<string>
  }
}
