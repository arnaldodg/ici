export interface Registration {
  pending?: boolean;    // Waiting for registration ACK
  activated?: boolean;  // Password created!
  cancelled?: boolean;  // User cancelled registration request
  denied?: boolean;     // request not granted by backOffice admin

  phoneValidated?: boolean;
  
  userName?: string;
  displayName?: string;

  cnpj?: string;
  email?: string;
  
}