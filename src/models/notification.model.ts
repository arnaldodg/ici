export interface notification {
  id?: number;
  title?: string;
  message?: string;
  hasSound?: boolean;
  data?: any; // notification payload specs?!
  
  timestamp?: Date;
}