import * as Common from '../providers/common.service';

import { Client, createClient } from '../models/client.model';
import { Estate, createEstate } from '../models/estate.model';
import { Operation} from '../models/operation.model';

export interface Proposal {
  uuid: string;
  timestamp: string;
  status: string; // create enum ['InProgress', 'Ready', 'Sent', 'Invalid', 'Cancelled', ... ] ???
    
  submitDate: Date | null;
  analysisDate: Date | null;
  approvalDate: Date | null;
  paymentDate: Date | null;

  // aggregation refs
  clientId: string | null;  
  estateId: string | null;
  operationId: string | null;

  client?: any;   // use as ref
  estate?: any;   // use as ref
  operation?: any;// use as ref

  // options
  requestedAmount: number;
  includeExpenses: boolean;

  paramsVersion: string;
  
}

export function createProposal(source?: Proposal): Proposal {
  
  return {
    uuid: Common.createUUID(),
    timestamp: new Date().toISOString(),
    status: 'sim',

    submitDate: null,
    analysisDate: null,
    approvalDate: null,
    paymentDate: null,

    // aggregation refs
    clientId: createClient().uuid,  
    estateId: createEstate().uuid,
    operationId: null,

    client: null,   // use as ref
    estate: null,   // use as ref
    operation: null,// use as ref

    // options
    requestedAmount: 10000,
    includeExpenses: true,

    paramsVersion: '',

  }
}

