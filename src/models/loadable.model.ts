export interface Loadable {
  loading: boolean;
  loaded: boolean;
  hasError: boolean;
  error: string;
}