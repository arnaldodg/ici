import * as Common from '../providers/common.service'

export interface Estate {
  uuid: string;
  valor: number;
  cep: number;
  uf: string;   // from lookupSvc
  tipo: string; // comercial | residencial

  // docs
  matricula: {url: string, thumb: string};    // url -> S3 bucket file URL, thumb -> S3 bucket thumb URL or localStorage/cached path
  fotos: Array<string>; // urls -> melhor um obj

  clientId: string; // uuid ref
}

export function createEstate(source?: Estate){
  let hasSource = (typeof(source)!=='undefined' && source!==null);

  return {
    uuid: Common.createUUID(),
    valor: hasSource ? source.valor : 250000,
    cep: hasSource ? source.cep : 31000000,
    uf: hasSource ? source.uf : 'MG',
    tipo: hasSource ? source.tipo : 'R',

    matricula: hasSource ? source.matricula : {url: '', thumb: ''},
    fotos: hasSource ? source.fotos : [],

    clientId: hasSource ? source.clientId : null
  }
}