interface Address {
  endereco: string;
  numero: number;
  complemento: string;
  cep: string;
}

export interface User {
  nome: string;
  telefone: string;
  email: string;
  endereco: Address;
  
  // UserAccount
  cnpjEntidade: number;
  username: string;
  activated: boolean; // ou Pending?!
}