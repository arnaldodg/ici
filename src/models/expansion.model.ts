import { Proposal } from '../models/proposal.model';

interface Parcela {

}

export interface Expansion {
  parcelas: { 
    byId: { [key: string]: Parcela } 
  };
  sumario: {
    valorSolicitado: number;
    valorRecebido: number;
    valorSeguro: number;
    dataInicial: Date;
    dataFinal: Date;
  };
  params: {
    tipo: 'SAC' | 'PRICE';
    valorImovel: number;
    valorSolicitado: number;
    taxaMensal: number;
    qtdParcelas: number;
    dataInicial: Date;
    iof: boolean;
    itbi: boolean;
    seguro: boolean;
  }
}