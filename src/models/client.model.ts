import * as Common from '../providers/common.service'

export interface Client {
  uuid: string;
  status: string;
  nome: string; // Razao Social se PJ
  cpfpj: string | null;
  renda: number | null;

  // apenas para PF
  nascimento: string | null;     // requerido para simulação -> limites de idade etc
  sexo: string;         // verificar relevância
  estadoCivil: string | null;  // verificar relevância

  estates: Array<string>; // uuid refs
  proposals: Array<string>; // uuid refs 
}

export function createClient(source?: Client): Client{
  let hasSource = typeof(source)!=='undefined' && source!==null;

  return {
    uuid: Common.createUUID(),
    status: 'new',
    nome: hasSource ? source.nome : '',
    cpfpj: hasSource ? source.cpfpj : '',
    renda: hasSource ? source.renda : 10000,
    nascimento: hasSource ? source.nascimento : new Date().toISOString(),
    sexo: hasSource ? source.sexo : '',
    estadoCivil: hasSource ? source.estadoCivil : null,

    estates: hasSource ? source.estates : [],
    proposals: hasSource ? source.proposals : [] 
  }
}