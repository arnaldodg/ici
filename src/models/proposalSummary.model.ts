export interface proposalSummary {
  operationName: string;
  valorSolicitado: number;
  valorImovel: number;
  taxaMensal: number;
  despesas: number;
  prazo: number;
  valorParcela: number;
  iof: number;
  valorSeguro: number;

  proposalId: string;
}
