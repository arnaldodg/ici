import { Operation } from './operation.model';
import { Selector } from './selector.model';

export interface OpParams { // SimFormParams!
  groups?: Array<string>;

  selGroups?: { 
    [key: string]: Selector
  };

  operations?: {
    [key: string]: Operation
  };
  
  version?: string;
}

// test IF

var x: OpParams = {
  selGroups: {
    PROD: {
      id: 'PROD',
      type: 'btn-group',
      cols: 1,
      label: '',
      options: [
        'credCASA',
        'credFLEX'
      ],
      selectors: {
        'credCASA': [
          'CRA'
        ],
        'credFLEX': [
          'CRE'
        ]
      }
    }
  },
  operations: {
    '386': {
      id: '386',
      name: '386 - CREDFLEX POS IPCA 1,50% SAC',
      label: '',
      level: 6,
      path: 'ACI.CRE.2DN.ENO.PF3.A10.386',
      group: 'OPER',
      type: 'list',
      rules: {
        imovelMax: 15000000,
        valorMin: 50000,
        valorMax: 10000000,
        prazoMin: 24,
        prazoMax: 180,
        taxaJurosAnualNominal: 18,
        valorMinPrestacao: 200,
        idadeMax: 69,
        idadePrazoMax: 74,
        codSeguradora: 27,
        codSeguradoraDFI: 27,
        codApolice: 27,
        codApoliceDFI: 27,
        codSeguradora2: 27,
        codSeguradora2DFI: 27,
        codApolice2: 27,
        codApolice2DFI: 27,
        ltv: 50,
        comprometimentoRenda: 30,
        itbi: false,
        iof: true,
        indice: 204,
        ltvTotalOperacao: true,
        taxaAdministracao: 0
      }
    }
  },
  version: 'xpto010292030'
}
